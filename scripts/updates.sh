#!/usr/bin/env bash
# snatched from archlabs iso long time ago
# modified by root.nix.dk
# https://www.gnu.org/licenses/gpl-3.0.html
#

BAR_ICON=""
ICON=/usr/share/icons/gnome/32x32/apps/system-software-update.png

function get_updates(){
    if hash yay &>/dev/null; then
        pkgs=$(checkupdates; yay -Qua)
    else
        pkgs=$(checkupdates)
    fi
    updates=$(echo ${pkgs[@]})
}

while true; do
    get_updates
    if hash notify-send &>/dev/null; then
        if [[ ${updates} -gt 50 ]]; then
            notify-send -u critical -i ${ICON} \
                        "Consider updating" "${updates} New package updates"
        elif [[ ${updates} -gt 25 ]]; then
            notify-send -u normal -i ${ICON} \
                        "Consider updating" "${updates} New package updates"
        elif [[ ${updates} -gt 2 ]]; then
            notify-send -u low -i ${ICON} "${updates} New package updates"
        fi

        reboot="(ucode|cryptsetup|linux|nvidia|mesa|systemd|wayland|xf86-video|xorg)"
        if [[ ${pkgs} =~ ${reboot} ]]; then
            notify-send --urgency=critical --expire-time=5000 "Update alert!" "Pending updates (may) require system restart."
        fi
    fi

    while [[ ${updates} -gt 0 ]]; do
        if [[ ${updates} -eq 1 ]]; then
            echo "$BAR_ICON  ${updates} Update"
        elif [[ ${updates} -gt 1 ]]; then
            echo "$BAR_ICON  ${updates} Updates"
        fi
        sleep 60
        get_updates
    done

    while [[ ${updates} -eq 0 ]]; do
        ehco ""
        sleep 3600
        get_updates
    done
done
