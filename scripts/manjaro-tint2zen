#!/usr/bin/env bash

# tint2zen: a tint2 selection and switcher script
# Copyright (C) 2015 damo <damo@bunsenlabs.org>
# Copyright (C) 2017 Nathaniel Maia <natemaia10@gmail.com>
#
# forked by manjaro <fhatmanjaroorg>
#

readonly TITLE="Tint2 Manager"
readonly ICON="--window-icon=/usr/share/icons/manjaro/maia/48x48.png"
readonly CHECKLIST="zenity --width=400 --height=500 $ICON --list --checklist --multiple"
readonly QUESTION="zenity --question $ICON --width=300"
readonly CONFIG_PATH="$HOME/.config/tint2"
readonly AUTOSTART="$HOME/.config/openbox/autostart"
readonly USAGE="USAGE: [OPTIONS] [SESSIONFILE]
\n\t-z, --session\tUse entered SESSIONFILE rather than default
\t-h, --help\tPrint this usage message and exit
\nWith no command argument, the script uses the current WM session file
\n\t\t'$CONFIG_PATH/sessions/$WM-sessionfile'
\nTo start session at login, add the following line to autostart:
\tsleep 1; manjaro-tint2-session &"

readonly COMMON_INCLUDE="/usr/lib/manjaro/common/include.cfg"
if ! . "$COMMON_INCLUDE" 2>/dev/null; then
    echo $"Error: Failed to source $COMMON_INCLUDE" >&2 ; exit 1
fi

declareDependencies tint2 zenity find read

if ! [[ $WM ]]; then
    readonly window_managers=(bspwm i3 openbox xfce awesome)
    for i in "${window_managers[@]}"; do
        if [[ $(wmctrl -m | grep -i name | awk '{print tolower($2)}') == "$i" ]]; then
            readonly WM=$i ; break
        elif [[ $(xprop -root -notype | grep "WM_NAME =" | tr -d '"' | awk '{print tolower($3)}') == "$i" ]]; then
            readonly WM=$i ; break
        elif [[ $(awk '{print tolower($0)}' <<< "$XDG_CURRENT_DESKTOP") == "$i" ]]; then
            readonly WM=$i ; break
        fi
    done
fi

# SESSIONFILE is mutable by user
if [[ $WM ]]; then
    SESSIONFILE="$CONFIG_PATH/sessions/$WM-sessionfile"
else
    SESSIONFILE="$CONFIG_PATH/sessions/sessionfile"
fi
case $1 in
    -h|-H|--help) echo -e "$USAGE" && exit 0 ;;
    -z|--session)
        if [[ $2 ]]; then
            SESSIONFILE="$2"
        else
            echo "Invalid Session.. Exiting" && exit 2
        fi
esac

if ! [[ -d $CONFIG_PATH ]] && [[ -d /etc/skel/.config/tint2 ]]; then
    cp -rf /etc/skel/.config/tint2 "$HOME/.config/"
elif ! [[ -d $CONFIG_PATH ]]; then
    mkdir -p "$CONFIG_PATH"
fi

running_tints() {
    if [[ $(pidof tint2) ]]; then
        TEMPFILE=$(mktemp --tmpdir tint2s.XXXX)
        pgrep -a tint2 > "$TEMPFILE"
    fi
}

readonly FILES=($(find -L "$CONFIG_PATH" -maxdepth 4 -type f))
find_tints() {
    LIST=""
    for f in "${FILES[@]}"; do
        NAME=$(basename "$f")
        if [[ $NAME = *tintrc ]] || [[ $NAME == 'tint2rc' ]] || [[ $NAME = *tint2rc ]] || grep -q 'panel_monitor' "$f" && [[ ! $NAME =~ "backup" ]]
        then
            if [[ -e $TEMPFILE ]] && grep -q "$f" "$TEMPFILE"; then
                LIST="$LIST TRUE $NAME"
            else
                LIST="$LIST FALSE $NAME"
            fi
        fi
    done
}

create_session() {
    mkdir -p $CONFIG_PATH/sessions; echo -e "# Tint2 $WM Sessionfile
# DO NO edit this file, it will be overwritten by manjaro-tint2zen
# Make a custom sessionfile instead and use -z or --session FILE option
# To load session at startup use the following line\n\n# manjaro-tint2-session &\n" > $SESSIONFILE
    for name in $ANSWER; do
        for file in "${FILES[@]}"; do
            if grep -q "$file" "$SESSIONFILE" || [[ $(basename "$file") != "$name" ]]; then
                continue
            else
                echo -e "$file" >> "$SESSIONFILE" ; break
            fi
        done
    done

    [[ $(pidof tint2) ]] && pkill tint2
    while read -r c; do
        if [[ "$c" =~ ^#.*$ ]] || ! [[ "$c" ]] || ! [[ -f "$c" ]]; then
            continue
        else
            tint2 -c "$c" &>/dev/null &
        fi
    done < "$SESSIONFILE"
    if hash manjaro-compositor &>/dev/null; then manjaro-compositor --restart; fi

    sleep 0.5
    if [[ $(pidof tint2) ]]; then
        if [[ -f $AUTOSTART ]] && ! grep -q "manjaro-tint2-session" "$AUTOSTART"; then
            if grep -q "tint2" "$AUTOSTART"; then
                sed -i '/tint2/ a sleep 1; manjaro-tint2-session &' "$AUTOSTART"
                # sed -i '/tint2/d' "$AUTOSTART"
            elif grep -q "polybar" "$AUTOSTART"; then
                sed -i '/polybar/ a sleep 1; manjaro-tint2-session &' "$AUTOSTART"
                # sed -i '/polybar/d' "$AUTOSTART"
            elif grep -q "nitrogen" "$AUTOSTART"; then
                sed -i '/nitrogen/ a sleep 1; manjaro-tint2-session &' "$AUTOSTART"
            else
                sed -i '1s/^/manjaro-tint2-session & \n/' "$AUTOSTART"
            fi
        fi
    fi
}

while ! [[ $ANSWER ]]; do
    running_tints
    find_tints
    MSG="<big><b>Select tint2 panels to launch</b></big>\n\nRunning panels will be check marked\n\nTo disable a panel:"
    MSG="$MSG <b>uncheck and click Ok</b>\n\nSession will be saved to: <b>$(basename "$SESSIONFILE")</b>\n"
    ANSWER=$($CHECKLIST --title="$TITLE" --text="$MSG" --column="Select" --column="Tint2" $LIST --separator=" ")
    if [[ $? == 1 ]]; then
        ANSWER=NONE
    elif ! [[ $ANSWER ]]; then
        if [[ $(pidof tint2) ]]; then
            MSG="<big><b>No Panels Selected</b></big>\n\nWhat would you like to do?"
            if $QUESTION --title="$TITLE" --ok-label="Go Back" --cancel-label="Stop Panels" --text="$MSG" &>/dev/null; then
                ANSWER="" ; continue
            else
                pkill tint2 ; echo "" > "$SESSIONFILE" ; ANSWER=NONE
            fi
        else
            ANSWER=NONE
        fi
    else
        create_session
    fi
    [[ -e $TEMPFILE ]] && rm -f "$TEMPFILE"
done

exit 0
